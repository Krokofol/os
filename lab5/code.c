#include <io.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>

typedef struct StringList StringList;

struct StringList{
    int pos;
    int length;
    StringList *next;
};

StringList* creatElement(int pos, int length){
    StringList *newElement;
    if (!(newElement = (StringList *)malloc(sizeof(StringList)))){
        printf("MEM_ERROR\n");
        return NULL;
    }
    newElement->pos = pos;
    newElement->length = length;
    newElement->next = NULL;
    return newElement;
}

StringList* addElement(StringList *list, int pos, int length) {
    StringList *boof = NULL;
    if (list == NULL)
        return creatElement(pos, length);
    list->next = addElement(list->next, pos, length);
    if (list->next == NULL)
        free(list);
    return list;
}

void deleteList(StringList *list){
    if (list != NULL){
        deleteList(list->next);
        free(list);
    }
    return;
}

void printString(int num, StringList *list, int fd){
    char string[256];
    int i;
    for (i = 1; i < num; i++) {
        list = list->next;
        if (list == NULL){
            printf("BAD_NUMBER\n");
            return;
        }
    }
    lseek(fd, list->pos - list->length, 0);
    read(fd, string, list->length);
    string[list->length - 2] = '\n';
    string[list->length - 1] = '\0';
    printf("%s", string);
}

int getFile(char argc, char *argv[]){
    printf("!!!%d\n", argc);
    if (argc == 1)
        return open("text.txt", O_RDONLY);
    else
        return open(argv[1], O_RDONLY);
}

int main(char argc, char *argv[]) {
    //открытие файла
    int fd = 0;
    fd = getFile(argc, argv);
    if (fd == -1) {
        printf("FILE_OPEN_ERROR\n");
        return 1;
    }

    //считывание файла
    char string[256];
    int count, i, length = 0, pos = 0;
    StringList *list = NULL;
    while((count = read(fd, string, 256)) != 0) {
        for (i = 0; i < count; i++){
            length++;
            pos++;
            if (string[i] == '\n') {
                list = addElement(list, ++pos, ++length);
                if (list == NULL){
                    close(fd);
                    return 0;
                }
                length = 0;
            }
        }
    }
    list = addElement(list, 2 + pos, 2 + length);

    //поиск строк
    int stringNumber;
    while(1) {
        if (scanf("%d", &stringNumber) == 0){
            printf("SCANF_ERROR\n");
            break;
        }
        if (!stringNumber)
            break;
        printString(stringNumber, list, fd);
    }
    
    //завершение программы
    deleteList(list);
    close(fd);
    return 0;
}